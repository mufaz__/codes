// Given a positive integer n, you can apply one of the following operations:

// If n is even, replace n with n / 2.
// If n is odd, replace n with either n + 1 or n - 1.
// Return the minimum number of operations needed for n to become 1.

void intReplacement(int n, var count) {
  if (n == 1) {
    print(count);
    return;
  } else {
    if (n % 2 == 0) {
      n = n ~/ 2;
      count += 1;
      intReplacement(n, count);
    } else {
      n = n - 1;
      count += 1;
      intReplacement(n, count);
    }
  }
}

main(List<String> args) {
  int n = 7;
  var count = 0;
  intReplacement(n, count);
}
