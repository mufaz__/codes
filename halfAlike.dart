// You are given a string s of even length.
//  Split this string into two halves of equal lengths,
//   and let a be the first half and b be the second half.

// Two strings are alike if they have the same number of vowels ('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U').
//  Notice that s contains uppercase and lowercase letters.

// Return true if a and b are alike. Otherwise, return false.

main(List<String> args) {
  String string = "textbook";

  var len = (string.length / 2).round();
  String a = string.substring(0, len);
  String b = string.substring(len);
  print(compare(a, b));
}

bool compare(String a, String b) {
  String vowels = 'aeiouAEIOU';
  Map count = {"a": 0, "b": 0};
  for (var i = 0; i < a.length; i++) {
    if (vowels.contains(a[i])) {
      count["a"] += 1;
    }
    if (vowels.contains(b[i])) {
      count["b"] += 1;
    }
  }
  if (count["a"] != count["b"]) {
    return false;
  } else {
    return true;
  }
}
