'''
Given an integer n, 
count the total number of digit 1 appearing in
 all non-negative integers less than or equal to n.

'''
def digitCount(n):    
    count=0
    for i in range(n+1):
        for j in str(i):
            if j=='1':
                count+=1
                
    print(count)

digitCount(n=100)    