'''
Given an integer array nums,
in which exactly two elements appear only once
and all the other elements appear exactly twice.
Find the two elements that appear only once.
You can return the answer in any order.

'''




def singleNumber(numbers):
    count=dict()
    output=[]
    
    for num in numbers:
        count[num]=count.get(num,0)+1
    for k,v in count.items():
        if v==1:
            output.append(k)
    print(output)

numbers=[1,2,2,1,4,5,4,8,4]            
singleNumber(numbers)
