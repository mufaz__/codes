// Given two strings S and T, return if they are equal when both are typed into empty text editors.
//  # means a backspace character.

// Note that after backspacing an empty text, the text will continue empty

main(List<String> args) {
  compareString('#qw#wert#ty', 'q#qwe#erty#y');
}

void compareString(String s, String t) {
  List<String> outputS = [];
  List<String> outputT = [];

  s.split('').forEach((element) {
    if (element == '#') {
      if (outputS.isNotEmpty) {
        outputS.removeLast();
      }
    } else {
      outputS.add(element);
    }
  });
  t.split('').forEach((element) {
    if (element == '#') {
      if (outputS.isNotEmpty) {
        outputT.removeLast();
      }
    } else {
      outputT.add(element);
    }
  });
  s = outputS.join();
  t = outputT.join();
  if (s == t) {
    print('true');
  } else {
    print('false');
  }
}
