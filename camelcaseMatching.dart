// A query word matches a given pattern if we can insert lowercase letters to
// the pattern word so that it equals the query.
//  (We may insert each character at any position, and may insert 0 characters.)

// Given a list of queries, and a pattern, return an answer list of booleans,
//  where answer[i] is true if and only if queries[i] matches the pattern.

main(List<String> args) {
  List queries = [
    "FooBar",
    "FooBarTest",
    "FootBall",
    "FrameBuffer",
    "ForceFeedBack"
  ];
  String pattern = 'FoBa';

  matching(queries, pattern);
}

void matching(List query, String pattern) {
  String lc = 'qwertyuiopasdfghjklzxcvbnm';
  String uc = 'QWERTYUIOPASDFGHJKLZXCVBNM';
  List patterns = [];
  List temp = [];
  List output = [];
  for (var i = 0; i < pattern.length; i++) {
    if (temp.isEmpty) {
      temp.add(pattern[i]);
    } else if (uc.contains(pattern[i])) {
      patterns.add(temp.join(''));
      temp.clear();
      temp.add(pattern[i]);
    } else {
      temp.add(pattern[i]);
    }
  }
  if (temp.isNotEmpty) {
    patterns.add(temp.join(''));
    temp.clear();
  }
  for (var i = 0; i < query.length; i++) {
    output.add(camelcase(query[i], patterns));
  }
  print(output);
}

bool camelcase(String query, List patterns) {
  String lc = 'qwertyuiopasdfghjklzxcvbnm';
  String uc = 'QWERTYUIOPASDFGHJKLZXCVBNM';
  List listquery = [];
  List temp = [];

  for (var i = 0; i < query.length; i++) {
    if (temp.isEmpty) {
      temp.add(query[i]);
    } else if (uc.contains(query[i])) {
      listquery.add(temp.join(''));
      temp.clear();
      temp.add(query[i]);
    } else {
      temp.add(query[i]);
    }
  }
  if (temp.isNotEmpty) {
    listquery.add(temp.join(''));
    temp.clear();
  }
  if (patterns.length != listquery.length) {
    return false;
  } else {
    for (var i = 0; i < patterns.length; i++) {
      if (patterns[i] != listquery[i].substring(0, patterns[i].length)) {
        return false;
        break;
      }
    }
    return true;
  }
}
