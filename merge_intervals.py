'''
Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals,
 and return an array of the non-overlapping intervals that cover all the intervals in the input.
eg :-Input: intervals = [[1,4],[4,5]]
Output: [[1,5]]

'''


def mergeIntervals(input):
    output=[]
    for i in input:
        if output==[]:
            output.append(i)
        elif i[0]<= output[-1][1]:
            output[-1]=[output[-1][0],i[1]]    
        else:
            output.append(i)   
    print(output)


intervals = [[1, 3], [2, 6], [8, 10], [15, 18]]
mergeIntervals(intervals)
