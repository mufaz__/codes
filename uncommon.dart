// We are given two sentences A and B.  (A sentence is a string of space separated words.
//  Each word consists only of lowercase letters.)

// A word is uncommon if it appears exactly once in one of the sentences,
//  and does not appear in the other sentence.

// Return a list of all uncommon words.

// You may return the list in any order

main(List<String> args) {
  unCommon('this is a car', 'this is a bike');
}

void unCommon(String a, String b) {
  Map output = {};
  List uncommon = [];

  List string = a.split(' ') + b.split(' ');

  string.forEach((element) {
    if (output.containsKey(element)) {
      output.update(element, (value) => value + 1);
    } else {
      output.putIfAbsent(element, () => 1);
    }
  });
  output.forEach((key, value) {
    if (value == 1) {
      uncommon.add(key);
    }
  });
  print(uncommon);
}
