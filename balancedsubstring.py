
# print the count of longest substring of balanced brackets 
# in the given string,

#  eg:-string='((()){}]'
#  output :- 4



def longestBalancedBrackets(s):
    openbraces = ('{[(')
    templist = []
    balancedList = []
    count = dict()
    lastadded = ''

    for i in s.strip():
        if i in openbraces:
            if balancedList != []:
                count[''.join(balancedList)] = len(balancedList)
                balancedList.clear()
                templist.clear()
            templist.append(i)
            lastadded = i
        elif lastadded == '{' and i == '}' or lastadded == '[' and i == ']' or lastadded == '(' and i == ')':
            balancedList.append(templist.pop())
            balancedList.append(i)
            if templist:
                lastadded = templist[-1]
            else:
                count[''.join(balancedList)] = len(balancedList)
                balancedList.clear()
                templist.clear()
                lastadded = ''
        else:
            if balancedList:
                count[''.join(balancedList)] = len(balancedList)
                balancedList.clear()
            templist.clear()
            lastadded = ''
    biggestCount = 0
    for v in count.values():
        if v > biggestCount:
            biggestCount = v
    print(biggestCount)


string = '((()){]'
longestBalancedBrackets(string)
